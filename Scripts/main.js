(function () {

  angular
    .module("testModel", [])
    .directive("emailsEditor", emailsEditorDirective)
    .controller("emailsEditorController", emailsEditorController);

    function emailsEditorDirective() {

        return {
            restrict: 'E',
            replace: true,
            scope: {
                items: '='
            },
            templateUrl: 'templates/emails_editor_template.html',
            controller: function ($scope) {

              $scope.keypress = function($event) {
                $scope.lastkey = $event.keyCode;
                if (($event.keyCode == 13) || ($event.keyCode == 188) || ($event.keyCode == 191)) {

                  if ($scope.email != '') {

                    var valid = ( /^[\w\d%$:.-]+@\w+\.\w{2,5}$/.test($scope.email) );

                    $scope.items.push($scope.email);

                    $scope.email = '';
                  }
                }

                if (($event.keyCode == 8) && ($scope.email.length == 0)) {
                  $scope.items.splice($scope.items.length-1, 1);
                }
              };

              $scope.deleteEmail = function(item) {
                $scope.items.splice($scope.items.indexOf(item), 1);
              };

              $scope.emailBlockStyle = function(email) {
                if ( /^[\w\d%$:.-]+@\w+\.\w{2,5}$/.test(email) ) {
                  return 'span-chip';
                } else {
                  return 'no-valid-email';
                }
              }

            }
        };
    };

    function emailsEditorController($scope) {
      $scope.items = ["andreyalt86@gmail.ru", "agforadwqdqwwqw@mail.ru", "agforadwqwwqw@mail.ru"];

      $scope.addEmails = function() {

        var strValues="qwertyuiopasdfghjklzxcvbnm1234567890";
        var strEmail = "";
        var strTmp;
        for (var i=0;i<10;i++) {
            strTmp = strValues.charAt(Math.round(strValues.length*Math.random()));
            strEmail = strEmail + strTmp;
        }
        strTmp = "";
        strValues="qwertyuiopasdfghjklzxcvbnm";
        strEmail = strEmail + "@";
        for (var j=0; j<8; j++) {
            strTmp = strValues.charAt(Math.round(strValues.length*Math.random()));
            strEmail = strEmail + strTmp;
        }
        strEmail = strEmail + ".com"

        $scope.items.push(strEmail);
      }

      $scope.getEmailsCount = function() {
        alert($scope.items.length);
      }

    };
})();
